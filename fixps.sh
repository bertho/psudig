#!/bin/bash

PAGES=4
FILES="psudig_1.ps psudig_2.ps psudig_3.ps psuctl_1.ps"
BASE="psudig_sch"
TARGET="${BASE}.ps"
PDF="${BASE}.pdf"

for i in ${FILES}
do
	if [ ! -r "${i}" ]
	then
		echo "${i} not readable"
		exit 1
	fi
done

psmerge -o${TARGET} ${FILES}
sed -i -e's_^%%Title:.*_%%Title: Digitally Controlled PSU_' -e's,^%%Author:.*,%%Author: Vagrearg,' ${TARGET}
ps2pdf14 -dOptimize=true -dEmbedAllFonts=true -sPAPERSIZE=a4 ${TARGET}
for ((i=1; i<=${PAGES}; i++))
do
	pdftoppm -gray -png -f ${i} -l ${i} ${PDF} > ${BASE}_$i.png
	convert ${BASE}_$i.png -scale '25%' ${BASE}_$i-small.png
done
